package com.mycompany.lab3;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author informatics
 */
public class XOCheckgame {
    
    public XOCheckgame() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testcheckwin_X_Vertical1_true(){
            String[][] table = {{"X","2","3"},{"X","5","6"},{"X","8","9"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_Vertical2_true(){
            String[][] table = {{"1","X","3"},{"4","X","6"},{"7","X","9"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_Vertical3_true(){
            String[][] table = {{"1","2","X"},{"4","5","X"},{"7","8","X"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
     @Test
    public void testcheckwin_O_Vertical1_true(){
            String[][] table = {{"O","2","3"},{"O","5","6"},{"O","8","9"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_Vertical2_true(){
            String[][] table = {{"1","O","3"},{"4","O","6"},{"7","O","9"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_Vertical3_true(){
            String[][] table = {{"1","2","O"},{"4","5","O"},{"7","8","O"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_horizontal1_true(){
            String[][] table = {{"X","X","X"},{"4","5","6"},{"7","8","9"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_horizontal2_true(){
            String[][] table = {{"1","2","3"},{"X","X","X"},{"7","8","9"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_horizontal3_true(){
            String[][] table = {{"1","2","3"},{"4","5","6"},{"X","X","X"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_horizontal1_true(){
            String[][] table = {{"O","O","O"},{"4","5","6"},{"7","8","9"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_horizontal2_true(){
            String[][] table = {{"1","2","3"},{"O","O","O"},{"7","8","9"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_horizontal3_true(){
            String[][] table = {{"1","2","3"},{"4","5","6"},{"O","O","O"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_X_diagonal1_true(){
            String[][] table = {{"X","2","3"},{"4","X","6"},{"7","8","X"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
     @Test
    public void testcheckwin_X_diagonal2_true(){
            String[][] table = {{"1","2","X"},{"4","X","6"},{"X","8","9"}};
            String currentplayer = "X";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_diagonal1_true(){
            String[][] table = {{"O","2","3"},{"4","O","6"},{"7","8","O"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testcheckwin_O_diagonal2_true(){
            String[][] table = {{"1","2","O"},{"4","O","6"},{"O","8","9"}};
            String currentplayer = "O";
            boolean result =Lab3.checkwin(table,currentplayer);
            assertEquals(true,result);
    }
    @Test
    public void testDraw_true(){
            String[][] table = {{"X","O","X"},{"O","X","O"},{"O","X","O"}};
            boolean result =Lab3.draw(table);
            assertEquals(true,result);
    }
    
    }
    
